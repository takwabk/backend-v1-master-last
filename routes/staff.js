const express = require("express");
const router = express.Router();
const { Staff, validateStaff } = require("../models/staff");
// const authorize = require("../middleware/role");
// const authorization = require("../middleware/auth");

const asyncMiddleware = require("../middleware/async");

const validate = require("../middleware/validateRequest");

router.post("/staffs", [validate(validateStaff)], async (req, res) => {
  let staff = await Staff.findOne({
    matricule: req.body.matricule,
  });

  if (staff)
    return res.status(400).json({
      msgsrv: `${req.body.nom} with reference : ${req.body.matricule} already registered.`,
    });

  staff = new Staff(req.body);
  staff = await staff.save();

  res.send(staff);
});

router.get(
  "/staffs",
  [validate(validateStaff)],
  asyncMiddleware(async (req, res) => {
    const staff = await Staff.find().sort("createdOn");
    res.send(staff);
  })
);
module.exports = router;
