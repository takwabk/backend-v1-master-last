
const express = require("express");
const nodemailer = require("nodemailer");
const router = express.Router();
const { User, validateUser } = require("../models/user.model");

const validate = require("../middleware/validateRequest");
const asyncMiddleware = require("../middleware/async");
const Joi = require("joi");
const _ = require("lodash");
const bcrypt = require("bcrypt");

const authorization = require("../middleware/auth");
const isAdmin = require("../middleware/admin");

router.get("/verify/:token", async (req, res) => {
  console.log("userVerif");

  let token = req.params.token;

  const userVerif = await User.find({ activationToken: token });
  if (userVerif) {
    userVerif.activatedMail = true;
    await userVerif.save();
    console.log("hmmm verify tokrn hhh");
    res.redirect(" http://193.70.90.165/login");
  } else {
    res.status(404).json({ msgsrv: "User not found" });
  }
});
//Register
router.post("/register", validate(validateUser), async (req, res) => {
  //validators
  // const { error } = validateUser(req.body);
  // if (error) return res.status(400).send(error.details[0].message);

  //user already registered ??
  let user = await User.findOne({ email: req.body.email });
  if (user) return res.status(400).send({ msgsrv: "User already registered." });

  //create new user //bech teb9ach tekteb fi name:req.body.name ect...

  user = new User(
    _.pick(req.body, [
      "email",
      "password",
      "activatedMail",
      "activationToken",
      "nom",
      "prenom",
      "tel",
    ])
  );

  //hach password
  const salt = await bcrypt.genSalt(10);

  let activeToken = await bcrypt.hash(Date.now().toLocaleString(), salt);

  user.activationToken = activeToken;

  user.password = await bcrypt.hash(user.password, salt);

  console.log("user", user);
  //save user in database
  await user.save();
  //let verifURl = "http://localhost:3000/verify/" + activeToken;
  verifURl = "http://193.70.90.165/verify/" + activeToken;
  //send mail
  var smtpTransport = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465, // 587 kif tebda heberge ..465 local may9ele9ech ds tt cas 5ater nodemailer ye5dem 3azouz ken jet php nn
    //google:less secure app :active , l'email via user hetha
    //  auth: { user: "safwen.benfredj@gmail.com", pass: "01161590baba" },

    auth: { user: "bk.takwa123@gmail.com", pass: "Shadytexenim2021" },
    tls: {
      cipher: "SSLv3",
      rejectUnauthorized: false,
    }, // do not fail on invalid certs
  });
  let emailBody = `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www=
.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns=3D"http://www.w3=
.org/1999/xhtml" xmlns:o=3D"urn:schemas-microsoft-com:office:office" xmlns:=
v=3D"urn:schemas-microsoft-com:vml"><head>

<meta content=3D"text/html; charset=3Dutf-8" http-equiv=3D"Content-Type">
<meta content=3D"width=3Ddevice-width" name=3D"viewport">

<meta content=3D"IE=3Dedge" http-equiv=3D"X-UA-Compatible">

<title></title>
<style type=3D"text/css">
body {
margin: 0;
padding: 0;
}

table,
td,
tr {
vertical-align: top;
border-collapse: collapse;
}

* {
line-height: inherit;
}

a[x-apple-data-detectors=3Dtrue] {
color: inherit !important;
text-decoration: none !important;
}
</style>
<style id=3D"media-query" type=3D"text/css">
@media (max-width: 520px) {

.block-grid,
.col {
min-width: 320px !important;
max-width: 100% !important;
display: block !important;
}

.block-grid {
width: 100% !important;
}

.col {
width: 100% !important;
}

.col_cont {
margin: 0 auto;
}

img.fullwidth,
img.fullwidthOnMobile {
width: 100% !important;
}

.no-stack .col {
min-width: 0 !important;
display: table-cell !important;
}

.no-stack.two-up .col {
width: 50% !important;
}

.no-stack .col.num2 {
width: 16.6% !important;
}

.no-stack .col.num3 {
width: 25% !important;
}

.no-stack .col.num4 {
width: 33% !important;
}

.no-stack .col.num5 {
width: 41.6% !important;
}

.no-stack .col.num6 {
width: 50% !important;
}

.no-stack .col.num7 {
width: 58.3% !important;
}

.no-stack .col.num8 {
width: 66.6% !important;
}

.no-stack .col.num9 {
width: 75% !important;
}

.no-stack .col.num10 {
width: 83.3% !important;
}

.video-block {
max-width: none !important;
}

.mobile_hide {
min-height: 0px;
max-height: 0px;
max-width: 0px;
display: none;
overflow: hidden;
font-size: 0px;
}

.desktop_hide {
display: block !important;
max-height: none !important;
}

.img-container.big img {
width: auto !important;
}
}
</style>
<style id=3D"icon-media-query" type=3D"text/css">
@media (max-width: 520px) {
.icons-inner {
text-align: center;
}

.icons-inner td {
margin: 0 auto;
}
}
</style>
</head><body class=3D"clean-body" style=3D"margin: 0; padding: 0; -webkit-t=
ext-size-adjust: 100%; background-color: #FFFFFF;">

<table bgcolor=3D"#FFFFFF" cellpadding=3D"0" cellspacing=3D"0" class=3D"nl-=
container" role=3D"presentation" style=3D"table-layout: fixed; vertical-ali=
gn: top; min-width: 320px; border-spacing: 0; border-collapse: collapse; ms=
o-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #FFFFFF; widt=
h: 100%;" valign=3D"top" width=3D"100%">
<tbody>
<tr style=3D"vertical-align: top;" valign=3D"top">
<td style=3D"word-break: break-word; vertical-align: top;" valign=3D"top">

<div style=3D"background-color:transparent;">
<div class=3D"block-grid" style=3D"min-width: 320px; max-width: 500px; over=
flow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margi=
n: 0 auto; background-color: #0eef7d;">
<div style=3D"border-collapse: collapse;display: table;width: 100%;backgrou=
nd-color:#0eef7d;">

<div class=3D"col num12" style=3D"min-width: 320px; max-width: 500px; displ=
ay: table-cell; vertical-align: top; width: 500px;">
<div class=3D"col_cont" style=3D"width:100% !important;">

<div style=3D"border-top:0px solid transparent; border-left:0px solid trans=
parent; border-bottom:0px solid transparent; border-right:0px solid transpa=
rent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left=
: 0px;">

<a href="${verifURl}" style=3D"text-decoration:none;display:inline-block;color:#ffffff;backg=
round-color:#3AAEE0;border-radius:4px;-webkit-border-radius:4px;-moz-border=
-radius:4px;width:auto; width:auto;;border-top:1px solid #3AAEE0;border-rig=
ht:1px solid #3AAEE0;border-bottom:1px solid #3AAEE0;border-left:1px solid =
#3AAEE0;padding-top:5px;padding-bottom:5px;font-family:Arial, Helvetica Neu=
e, Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:k=
eep-all;"><span style=3D"padding-left:20px;padding-right:20px;font-size:16p=
x;display:inline-block;letter-spacing:undefined;"><span style=3D"font-size:=
 16px; line-height: 2; word-break: break-word; mso-line-height-alt: 32px;">=
Activer</span></span></a>

</div>

</div>

</div>
</div>

</div>
</div>
</div>
<div style=3D"background-color:transparent;">
<div class=3D"block-grid" style=3D"min-width: 320px; max-width: 500px; over=
flow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margi=
n: 0 auto; background-color: transparent;">
<div style=3D"border-collapse: collapse;display: table;width: 100%;backgrou=
nd-color:transparent;">

<div class=3D"col num12" style=3D"min-width: 320px; max-width: 500px; displ=
ay: table-cell; vertical-align: top; width: 500px;">
<div class=3D"col_cont" style=3D"width:100% !important;">

<div style=3D"border-top:0px solid transparent; border-left:0px solid trans=
parent; border-bottom:0px solid transparent; border-right:0px solid transpa=
rent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left=
: 0px;">

<table cellpadding=3D"0" cellspacing=3D"0" role=3D"presentation" style=3D"t=
able-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse=
: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign=3D"top" w=
idth=3D"100%">
<tr style=3D"vertical-align: top;" valign=3D"top">
<td align=3D"center" style=3D"word-break: break-word; vertical-align: top; =
padding-top: 5px; padding-right: 0px; padding-bottom: 5px; padding-left: 0p=
x; text-align: center;" valign=3D"top">

<table cellpadding=3D"0" cellspacing=3D"0" class=3D"icons-inner" role=3D"pr=
esentation" style=3D"table-layout: fixed; vertical-align: top; border-spaci=
ng: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: =
0pt; display: inline-block; margin-right: -4px; padding-left: 0px; padding-=
right: 0px;" valign=3D"top">

<tr style=3D"vertical-align: top;" valign=3D"top">
<td align=3D"center" style=3D"word-break: break-word; vertical-align: top; =
text-align: center; padding-top: 5px; padding-bottom: 5px; padding-left: 5p=
x; padding-right: 6px;" valign=3D"top"><a href=3D"https://www.designedwithb=
ee.com/"><img align=3D"center" alt=3D"Designed with BEE" class=3D"icon" hei=
ght=3D"32" src=3D"https://d15k2d11r6t6rl.cloudfront.net/public/users/Integr=
ators/BeeProAgency/53601_510656/Signature/bee.png" style=3D"border:0;" widt=
h=3D"null"></a></td>
<td style=3D"word-break: break-word; font-family: Arial, Helvetica Neue, He=
lvetica, sans-serif; font-size: 15px; color: #9d9d9d; vertical-align: middl=
e; letter-spacing: undefined;" valign=3D"middle"><a href=3D"https://www.des=
ignedwithbee.com/" style=3D"color:#9d9d9d;text-decoration:none;">Designed w=
ith BEE</a></td>
</tr>
</table>
</td>
</tr>
</table>

</div>

</div>
</div>

</div>
</div>
</div>

</td>
</tr>
</tbody>
</table>
</body></html>`;
  var mailOptions = {
    to: user.email,
    from: "safwen.benfredj@gmail.com",
    subject: "Activation compte",
    html: `<div><h1>Activation</h1><br/>merci de verifier votre compte en cliquant ici <a href =${verifURl}>verfi</a>  </div>`,
  };

  try {
    await smtpTransport.sendMail(mailOptions);
  } catch (err) {
    console.log(err.message);
  }
  const token = user.generateAuthToken();

  //res.send({email:user.email,name:user.name}) //no password !
  res.header("x-auth-token", token).send(_.pick(user, ["_id", "email", "nom"]));
});

//Login
router.post("/login", validate(validateEmailPwd), async (req, res) => {
  // const { error } = validateEmailPwd(req.body);
  // if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ email: req.body.email });
  if (!user) return res.status(400).send({ msgsrv: "Invalid email" });

  const validPassword = await bcrypt.compare(req.body.password, user.password);
  if (!validPassword)
    return res.status(400).send({ msgsrv: "Invalid password." });

  const token = user.generateAuthToken();
  res.send({ token: token });
});

//Profil

// not :/id bcz u can send another id and show profil of other users dont return pwd to user =>select
router.get("/profile", [authorization], async (req, res) => {
  console.log(req.user._id);
  const user = await User.findById(req.user._id).select("-password");
  res.status(200).json(user);
});
// add role and create user

router.post(
  "/addUser",

  [authorization, isAdmin, validate(validateUser)],
  async (req, res) => {
    console.log("user", req.body);

    user = new User({
      matricule: req.body.matricule,
      nom: req.body.nom,
      prenom: req.body.prenom,
      password: req.body.password,
      confirm_password: req.body.confirm_password,
      email: req.body.email,
      fonction: req.body.fonction,
      manager: req.body.manager,
      dateEmbauche: req.body.dateEmbauche,
      Complement_adresse: req.body.Complement_adresse,

      role: req.body.role,
    });
    //hach password
    const salt = await bcrypt.genSalt(10);

    user.password = await bcrypt.hash(user.password, salt);
    await user.save();
    res.status(200).json({
      msgsrv: `${user.nom} with email : ${user.email} registred with success.`,
    });
  }
);

router.get(
  "/getUserById",

  asyncMiddleware(async (req, res) => {
    const user = await User.findById(req.params.id);

    if (!user)
      return res.status(404).send("The user with the given ID was not found.");

    res.send(user);
  })
);

function validateEmailPwd(req) {
  const schema = {
    email: Joi.string().min(5).max(255).required().email(),
    password: Joi.string().min(5).max(255).required(),
  };

  return Joi.validate(req, schema);
}
router.get(
  "/getAllUser",
  asyncMiddleware(async (req, res) => {
    const users = await User.find().sort("createdOn");
    res.send(users);
  })
);
module.exports = router;






























// const express = require("express");
// const router = express.Router();
// const { User, validateUser } = require("../models/user.model");

// const validate = require("../middleware/validateRequest");
// const asyncMiddleware = require("../middleware/async");
// const Joi = require("joi");
// const _ = require("lodash");
// const bcrypt = require("bcrypt");

// const authorization = require("../middleware/auth");
// const isAdmin = require("../middleware/admin");

// //Profil

// // not :/id bcz u can send another id and show profil of other users dont return pwd to user =>select
// router.get("/profile", [authorization, isAdmin], async (req, res) => {
//   const user = await User.findById(req.user._id).select("-password");
//   res.send(user);
// });
// // add role and create user

// router.post(
//   "/addUser",

//   [authorization, isAdmin, validate(validateUser)],
//   async (req, res) => {
//     console.log("user", req.body);

//     user = new User({
//       matricule: req.body.matricule,
//       nom: req.body.nom,
//       prenom: req.body.prenom,
//       password: req.body.password,

//       email: req.body.email,
//       fonction: req.body.fonction,
//       manager: req.body.manager,
//       dateEmbauche: req.body.dateEmbauche,
//       Complement_adresse: req.body.Complement_adresse,

//       role: req.body.role,
//     });
//     //hach password
//     const salt = await bcrypt.genSalt(10);

//     user.password = await bcrypt.hash(user.password, salt);
//     await user.save();
//     res.status(200).json({
//       msgsrv: `${user.nom} with email : ${user.email} registred with success.`,
//     });
//   }
// );
// //Register
// router.post("/register", validate(validateUser), async (req, res) => {
//   //validators
//   // const { error } = validateUser(req.body);
//   // if (error) return res.status(400).send(error.details[0].message);

//   //user already registered ??
//   let user = await User.findOne({ email: req.body.email });
//   if (user) return res.status(400).send({ msgsrv: "User already registered." });

//   //create new user //bech teb9ach tekteb fi name:req.body.name ect...
//   user = new User(_.pick(req.body, ["email", "password"]));

//   //hach password
//   const salt = await bcrypt.genSalt(10);

//   user.password = await bcrypt.hash(user.password, salt);

//   //save user in database
//   await user.save();

//   const token = user.generateAuthToken();

//   //res.send({email:user.email,name:user.name}) //no password !
//   res.header("x-auth-token", token).send(_.pick(user, ["_id", "email"]));
// });

// //Login
// router.post("/login", validate(validateEmailPwd), async (req, res) => {
//   // const { error } = validateEmailPwd(req.body);
//   // if (error) return res.status(400).send(error.details[0].message);

//   let user = await User.findOne({ email: req.body.email });
//   if (!user) return res.status(400).send({ msgsrv: "Invalid email" });

//   const validPassword = await bcrypt.compare(req.body.password, user.password);
//   if (!validPassword)
//     return res.status(400).send({ msgsrv: "Invalid password." });

//   const token = user.generateAuthToken();
//   res.send({ token: token });
// });

// router.get(
//   "/getUserById",

//   asyncMiddleware(async (req, res) => {
//     const user = await User.findById(req.params.id);

//     if (!user)
//       return res.status(404).send("The user with the given ID was not found.");

//     res.send(user);
//   })
// );

// function validateEmailPwd(req) {
//   const schema = {
//     email: Joi.string().min(5).max(255).required().email(),
//     password: Joi.string().min(5).max(255).required(),
//   };

//   return Joi.validate(req, schema);
// }
// router.get(
//   "/getAllUser",
//   asyncMiddleware(async (req, res) => {
//     const users = await User.find().sort("createdOn");
//     res.send(users);
//   })
// );
// module.exports = router;
