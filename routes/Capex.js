const express = require("express");
const router = express.Router();
const { CAPEX } = require("../models/capex");

const asyncMiddleware = require("../middleware/async");

router.post(
  "/capex",

  asyncMiddleware(async (req, res) => {
    let capex = await CAPEX.findOne();

    // if (donnees)
    //   return res.status(400).json({
    //     msgsrv: `already registered.`,
    //   });

    capex = new CAPEX(req.body);
    capex = await capex.save();
    console.log("req",req.body);
    res.send(capex);
    
  })
);
router.post(
  "/updateCapex/:id",

  asyncMiddleware(async (req, res) => {
    
    const capex = await Capex.findByIdAndUpdate(
      req.params.id,
      { ...req.body },
      { new: true }
    );
console.log("req",req.body);
    if (!capex)
      return res
        .status(404)
        .send("The conception with the given ID was not found.");

    res.send(capex);
  })
);

router.get(
  "/getcapex",
  asyncMiddleware(async (req, res) => {
    const capex = await Capex.find();
    //  result = JSON.parse(JSON.stringify(fiche))

    res.send(capex);
  })
);
module.exports = router;
