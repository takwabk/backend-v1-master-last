const express = require("express");
const router = express.Router();
const { Donnee } = require("../models/DonneeConstruction");

const asyncMiddleware = require("../middleware/async");

router.post(
  "/donnees",

  asyncMiddleware(async (req, res) => {
  console.log("hhhhhh",req.body)

    donnees = new Donnee(req.body);
    donnees = await donnees.save();

    res.status(200).json(donnees);
  })
);
router.post(
  "/updateDate/:id",

  asyncMiddleware(async (req, res) => {
    
    const donnees = await Donnee.findByIdAndUpdate(
      req.params.id,
      { ...req.body },
      { new: true }
    );

    if (!donnees)
      return res
        .status(404)
        .send("The conception with the given ID was not found.");

    res.send(donnees);
  })
);

router.get(
  "/getDonnees",
  asyncMiddleware(async (req, res) => {
    const donnees = await Donnee.find();
    //  result = JSON.parse(JSON.stringify(fiche))

    res.send(donnees);
  })
);
module.exports = router;
