var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var CapexSchema = new Schema({
    //Conception
    Interet_porteur_projet:String,
    Etude_biomasse_projet:String,
    Etude_fonciere_projet:String,
    sercurisation_gaziers:String,
    valorisation_digestat:String,
    Realisation_Marketing:String,
    acceptabilite_projet:String,
    realisation_Projet:String,
    obtention_autorisations_urbanisme_exploitation_Projet:String,
    obtention_subvention_Projet:String,
    obtention_financement_Projet:String,
    Sous_Total1:String,
    //Construction
Ouverture_chantier:String,
Lot_1_Process_methanisation:String,
Lot_2_Valorisation:String,
Lot_3_Terrassement_Grande_Masse_Talutage_VRD:String,
Lot_4_Genie_Civil_circulaire_digestat_liquide_couverture_simple:String,
Lot_5_Genie_Civil_ouvrages_peripheriques:String,
Lot_6_Charpente_Batiment_Couverture:String,
Lot_7_Courant_fort_Soutirage_Electricite_Generale:String,
Maitrise_oevre:String,
Assistance_maitrise_ouvrage:String,
Materiel:String,
Sous_Total2:String,
Sous_Total3:String,
prix_total_123:String,

});

const Capex = mongoose.model("Capex", CapexSchema);

exports.CapexSchema = CapexSchema;
exports.Capex = Capex;