const config = require("config");
const jwt = require("jsonwebtoken");
const Joi = require("joi");
const mongoose = require("mongoose");

const { role } = require("../models/role");
console.log("roles from auth model", role);
const userSchema = new mongoose.Schema({
  nom: String,
  email: String,
  password: String,
  confirm_password: String,
  tel: String,
  activatedMail: { type: Boolean, default: false },
  activationToken: { type: String, default: "" },
  role: {
    type: String,
    default: "User",
  },
  matricule: String,
  prenom: String,

  fonction: String,
  manager: String,
  dateEmbauche: String,
  Complement_adresse: String,
  createdOn: { type: Date, default: Date.now },
});

userSchema.methods.generateAuthToken = function () {
  const token = jwt.sign(
    { _id: this._id, role: this.role },
    config.get("jwtPrivateKey")
  );
  return token;
};

const User = mongoose.model("User", userSchema);
//email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }) + complexity joi pws
function validateUser(user) {
  const schema = {
    role: Joi.string()
      .valid(...role)
      .required(),
    email: Joi.string().min(5).max(255).required().email(),
    password: Joi.string().min(5).max(255).required(),

    confirm_password: Joi.string(),
    tel: Joi.string(),
    matricule: Joi.string().empty(""),
    prenom: Joi.string(),

    nom: Joi.string().required(),

    fonction: Joi.string(),
    manager: Joi.string(),
    dateEmbauche: Joi.string(),
    Complement_adresse: Joi.string(),
  };
  return Joi.validate(user, schema);
}

exports.userSchema = userSchema;
exports.User = User;
exports.validateUser = validateUser;





























// const config = require("config");
// const jwt = require("jsonwebtoken");
// const Joi = require("joi");
// const mongoose = require("mongoose");

// const { role } = require("../models/role");
// console.log("roles from auth model", role);
// const userSchema = new mongoose.Schema({
//   email: String,
//   password: String,
//   role: {
//     type: String,
//     default: "User",
//   },
//   matricule: String,
//   prenom: String,

//   nom: String,

//   fonction: String,
//   manager: String,
//   dateEmbauche: String,
//   Complement_adresse: String,
//   createdOn: { type: Date, default: Date.now },
// });

// userSchema.methods.generateAuthToken = function () {
//   const token = jwt.sign(
//     { _id: this._id, role: this.role },
//     config.get("jwtPrivateKey")
//   );
//   return token;
// };

// const User = mongoose.model("User", userSchema);
// //email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }) + complexity joi pws
// function validateUser(user) {
//   const schema = {
//     role: Joi.string()
//       .valid(...role)
//       .required(),
//     email: Joi.string().min(5).max(255).required().email(),
//     password: Joi.string().min(5).max(255).required(),
//     matricule: Joi.string().empty(""),
//     prenom: Joi.string().required(),

//     nom: Joi.string().required(),

//     fonction: Joi.string().required(),
//     manager: Joi.string().required(),
//     dateEmbauche: Joi.string().required(),
//     Complement_adresse: Joi.string().required(),
//   };
//   return Joi.validate(user, schema);
// }

// exports.userSchema = userSchema;
// exports.User = User;
// exports.validateUser = validateUser;
