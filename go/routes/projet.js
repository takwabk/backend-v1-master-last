const express = require("express");
const router = express.Router();
const { Projet, validateProjet } = require("../models/projet");

const asyncMiddleware = require("../middleware/async");

const validate = require("../middleware/validateRequest");

router.post(
  "/projets",
  [ validate(validateProjet)],
  async (req, res) => {

    let projet = await Projet.findOne({
      ref: req.body.ref,
    });

    if (projet) return res.status(400).json({msgsrv:`${req.body.nom} with reference : ${req.body.ref} already registered.`}
      );

      projet = new Projet(req.body);
      projet = await projet.save();

    res.send(projet);
  }
);

router.get(
  "/projets",
  asyncMiddleware(async (req, res) => {
    const projet = await Projet.find().sort("createdOn");
    res.send(projet);
  })
);
module.exports = router;