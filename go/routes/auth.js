const express = require("express");
const router = express.Router();
const { User, validateUser } = require("../models/user.model");

const validate = require("../middleware/validateRequest");
const asyncMiddleware = require("../middleware/async");
const Joi = require("joi");
const _ = require("lodash");
const bcrypt = require("bcrypt");

const authorization = require("../middleware/auth");
const isAdmin = require("../middleware/admin");

//Profil

// not :/id bcz u can send another id and show profil of other users dont return pwd to user =>select
router.get("/profile", [authorization, isAdmin], async (req, res) => {
  const user = await User.findById(req.user._id).select("-password");
  res.send(user);
});
// add role and create user

router.post(
  "/addUser",

  [authorization, isAdmin, validate(validateUser)],
  async (req, res) => {
    console.log("user", req.body);

    user = new User({
      matricule: req.body.matricule,
      nom: req.body.nom,
      prenom: req.body.prenom,
      password: req.body.password,

      email: req.body.email,
      fonction: req.body.fonction,
      manager: req.body.manager,
      dateEmbauche: req.body.dateEmbauche,
      Complement_adresse: req.body.Complement_adresse,

      role: req.body.role,
    });
    //hach password
    const salt = await bcrypt.genSalt(10);

    user.password = await bcrypt.hash(user.password, salt);
    await user.save();
    res.status(200).json({
      msgsrv: `${user.nom} with email : ${user.email} registred with success.`,
    });
  }
);
//Register
router.post("/register", validate(validateUser), async (req, res) => {
  //validators
  // const { error } = validateUser(req.body);
  // if (error) return res.status(400).send(error.details[0].message);

  //user already registered ??
  let user = await User.findOne({ email: req.body.email });
  if (user) return res.status(400).send({ msgsrv: "User already registered." });

  //create new user //bech teb9ach tekteb fi name:req.body.name ect...
  user = new User(_.pick(req.body, ["email", "password"]));

  //hach password
  const salt = await bcrypt.genSalt(10);

  user.password = await bcrypt.hash(user.password, salt);

  //save user in database
  await user.save();

  const token = user.generateAuthToken();

  //res.send({email:user.email,name:user.name}) //no password !
  res.header("x-auth-token", token).send(_.pick(user, ["_id", "email"]));
});

//Login
router.post("/login", validate(validateEmailPwd), async (req, res) => {
  // const { error } = validateEmailPwd(req.body);
  // if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ email: req.body.email });
  if (!user) return res.status(400).send({ msgsrv: "Invalid email" });

  const validPassword = await bcrypt.compare(req.body.password, user.password);
  if (!validPassword)
    return res.status(400).send({ msgsrv: "Invalid password." });

  const token = user.generateAuthToken();
  res.send({ token: token });
});

router.get(
  "/getUserById",

  asyncMiddleware(async (req, res) => {
    const user = await User.findById(req.params.id);

    if (!user)
      return res.status(404).send("The user with the given ID was not found.");

    res.send(user);
  })
);

function validateEmailPwd(req) {
  const schema = {
    email: Joi.string().min(5).max(255).required().email(),
    password: Joi.string().min(5).max(255).required(),
  };

  return Joi.validate(req, schema);
}
router.get(
  "/getAllUser",
  asyncMiddleware(async (req, res) => {
    const users = await User.find().sort("createdOn");
    res.send(users);
  })
);
module.exports = router;
