var mongoose = require("mongoose");
var Schema = mongoose.Schema;
base_prix = ["MB", "MS"];
var ficheSchema = new Schema({
  createdOn:{ 
    type:Date,
    default:Date.now
  },
  somme_MS:String,
  somme_MB:String,
  volume_Nm3_CH4_an:String,
  somme_KWH:String,
  IC1: {
    type: String,
  },
  IC2: {
    type: String,
  },
  IC3: {
    type: String,
  },
  Description: {
    type: String,
  },
  Effluent_Delevage: {
    type: String,
  },
  unite: {
    type: String,
  },

  Taux_de_Presence_Rendement_par_ha: {
    type: String,
  },

  uMB_an: {
    type: String,
  },

  Unitee: {
    type: String,
  },

  Type: {
    type: String,
  },

  MS: {
    type: String,
  },

  MO_MS: {
    type: String,
  },

  Nm3_CH4_t_MO: {
    type: String,
  },
  Commentaire: {
    type: String,
  },

  Nm3_CH4_t_MB: {
    type: String,
  },
  Biogaz_m3_tMO: {
    type: String,
  },
  CH4percentMS: {
    type: String,
  },

  Nm3_CH4_m3_tMO: {
    type: String,
  },

  u_N_t: {
    type: String,
  },

  u_P2O5_t: {
    type: String,
  },

  u_K2O_t: {
    type: String,
  },
  t_MB_an: {
    type: String,
  },
  t_MS_an: {
    type: String,
  },
  Nm3_CH4_an: {
    type: String,
  },
  quantite:String,
  description:String,
  KWe_h:String,

  TOTAL_TMB: {
    type: String,
  },
  S_tt_exp_II: {
    type: String,
  },
  S_tt_Autre_III: {
    type: String,
  },
  GR_tt: {
    type: String,
  },
  S_tt: {
    type: String,
  },
  ration: {
    type: String,
  },
  N: {
    type: String,
  },
  P: {
    type: String,
  },
  K: {
    type: String,
  },
  //
  base_prix: {
    type: String,
    default: "MB",
  },
  prix: {
    type: String,
  },
  t_MB: {
    type: String,
  },
  t_MS: {
    type: String,
  },
  prix_intrants_MB: {
    type: String,
  },
  prix_intrants_MS: {
    type: String,
  },
  //table Produit
  volume_Nm3_CH4_an: {
    type: String,
  },
  tt_heure_annee: {
    type: String,
    default: "8760",
  },
  debit_horaire_Nm3_CH4_an: {
    type: String,
  },
  debit_horaire_injecte_Nm3_CH4_an: {
    type: String,
  },
  pvoir_calorfiq_sup_CH4: {
    type: String,
  },
  s_tt_KWH_h: {
    type: String,
  },
  heure_fonction: {
    type: String,
    default: "8200",
  },
  enrg_KWH_an: {
    type: String,
  },
  enrg_MWH_an: {
    type: String,
  },
  enrg_GWH_an: {
    type: String,
  },

  Tarif_debut_contrat1: {
    type: String,
  },
  Tarif_debut_contrat2: {
    type: String,
  },
  Recette_vente_biomethane: {
    type: String,
  },
  vente_digestat: {
    type: String,
  },
  Prix_digestat_Liquide: String,
  Prix_digestat_Solide: String,
  Quantite_digestat_Liquide: String,
  Quantite_digestat_Solise: String,
  Recette_vente_digestat: String,
  //Scenario
  installation_beneficie_aide_Agence_environnement_maitrise_energie: String,
  Date_signature_contrat_achat_biomethane: String,
  tarif_debut_contrat: String,
  Financement_Fonds_Propres: String,
  Pourcentage_FP_Financer: String,
  Montant_finance_FP_Finance: String,
  taux_interet_obligatoire: String,
  Montant_pret_bancaire: String,
  ERIBOR: String,
  Taux_interet_annuel: String,
  Montant_annuel_redevance: String,
  fumier: String,
  Mois_fumiere: String,
  mois_Silo: String,
  Densite_Silo: String,
  Densite_Fumier: String,
  hauteur_Silo: String,
  hauteur_Fumier: String,
  pluviometre: String,
  evaporation: String,
  tas_ensilage_Fumier: String,
  tas_ensilage_Silo: String,
  recup_eaux_uses_Fumier: String,
  recup_eaux_uses_Silo: String,
  total_eaux_uses: String,
  culture: String,
  autres: String,
  voie_unite: String,
  //il manque les attributs de feuille digestat ...
 
});

const Fiche = mongoose.model("Fiche", ficheSchema);
function validateFiche(fiche) {
  const schema = {
    base_prix: Joi.string().valid(...base_prix),
  };
  return Joi.validate(fiche, schema);
}

exports.ficheSchema = ficheSchema;
exports.Fiche = Fiche;
exports.validateFiche = validateFiche;












